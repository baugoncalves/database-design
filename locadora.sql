-- MariaDB dump 10.19  Distrib 10.5.15-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: locadora
-- ------------------------------------------------------
-- Server version	10.5.15-MariaDB-0+deb11u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `album`
--

DROP TABLE IF EXISTS `album`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `album` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `album`
--

LOCK TABLES `album` WRITE;
/*!40000 ALTER TABLE `album` DISABLE KEYS */;
INSERT INTO `album` VALUES (1,'Album 2022'),(2,'Álbum 2021'),(3,'Álbum 2020');
/*!40000 ALTER TABLE `album` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `artista`
--

DROP TABLE IF EXISTS `artista`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artista` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artista`
--

LOCK TABLES `artista` WRITE;
/*!40000 ALTER TABLE `artista` DISABLE KEYS */;
INSERT INTO `artista` VALUES (1,'Alok'),(2,'Dejavu'),(3,'Sandy e Júnior'),(4,'Mickel Jackson');
/*!40000 ALTER TABLE `artista` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cds`
--

DROP TABLE IF EXISTS `cds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(10) DEFAULT NULL,
  `nome` varchar(50) DEFAULT NULL,
  `valor_pago` decimal(6,2) DEFAULT NULL,
  `album_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `album_id` (`album_id`),
  CONSTRAINT `cds_ibfk_1` FOREIGN KEY (`album_id`) REFERENCES `album` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cds`
--

LOCK TABLES `cds` WRITE;
/*!40000 ALTER TABLE `cds` DISABLE KEYS */;
INSERT INTO `cds` VALUES (1,'123456','Thiller',12.50,1),(2,'22233','É Show',10.00,2),(3,'55585','As quatro estações',18.50,3),(4,'8995285','As melhores de Alok',28.90,2);
/*!40000 ALTER TABLE `cds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cidade`
--

DROP TABLE IF EXISTS `cidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `fk_estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_estado` (`fk_estado`),
  CONSTRAINT `cidade_ibfk_1` FOREIGN KEY (`fk_estado`) REFERENCES `estado` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cidade`
--

LOCK TABLES `cidade` WRITE;
/*!40000 ALTER TABLE `cidade` DISABLE KEYS */;
INSERT INTO `cidade` VALUES (1,'Teófilo Otoni',1),(2,'Ipatinga',1);
/*!40000 ALTER TABLE `cidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado`
--

DROP TABLE IF EXISTS `estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `sigla` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado`
--

LOCK TABLES `estado` WRITE;
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
INSERT INTO `estado` VALUES (1,'Minas Gerais','MG'),(2,'São Paulo','SP');
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `local_compra`
--

DROP TABLE IF EXISTS `local_compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `local_compra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rua` varchar(30) DEFAULT NULL,
  `bairro` varchar(20) DEFAULT NULL,
  `fk_cidade` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cidade` (`fk_cidade`),
  CONSTRAINT `local_compra_ibfk_1` FOREIGN KEY (`fk_cidade`) REFERENCES `cidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `local_compra`
--

LOCK TABLES `local_compra` WRITE;
/*!40000 ALTER TABLE `local_compra` DISABLE KEYS */;
INSERT INTO `local_compra` VALUES (1,'Submarino','Bairro teste',1),(2,'Segunda Loja','Bairro de lá',2);
/*!40000 ALTER TABLE `local_compra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `musicas`
--

DROP TABLE IF EXISTS `musicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `musicas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` int(2) DEFAULT NULL,
  `nome` varchar(30) DEFAULT NULL,
  `fk_cd` int(11) DEFAULT NULL,
  `tempo` time DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cd` (`fk_cd`),
  CONSTRAINT `musicas_ibfk_1` FOREIGN KEY (`fk_cd`) REFERENCES `cds` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `musicas`
--

LOCK TABLES `musicas` WRITE;
/*!40000 ALTER TABLE `musicas` DISABLE KEYS */;
INSERT INTO `musicas` VALUES (1,1,'Billy Jeans',1,'03:00:00'),(2,2,'Just bit it',1,'04:00:00'),(3,3,'Black or White',1,'05:00:00'),(4,1,'Dj Juninho Portugal',2,'01:50:00'),(5,1,'Turu turu turu',3,'03:50:00'),(6,1,'Alive',4,'02:50:00'),(7,2,'O que pensa que eu sou',2,'05:50:00'),(8,2,'On a big jet plane',4,'03:50:00');
/*!40000 ALTER TABLE `musicas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rel_local_compra_cds`
--

DROP TABLE IF EXISTS `rel_local_compra_cds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rel_local_compra_cds` (
  `fk_cds` int(11) NOT NULL,
  `fk_local_compra` int(11) NOT NULL,
  `data_compra` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`fk_cds`,`fk_local_compra`),
  KEY `fk_local_compra` (`fk_local_compra`),
  CONSTRAINT `rel_local_compra_cds_ibfk_1` FOREIGN KEY (`fk_cds`) REFERENCES `cds` (`id`),
  CONSTRAINT `rel_local_compra_cds_ibfk_2` FOREIGN KEY (`fk_local_compra`) REFERENCES `local_compra` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rel_local_compra_cds`
--

LOCK TABLES `rel_local_compra_cds` WRITE;
/*!40000 ALTER TABLE `rel_local_compra_cds` DISABLE KEYS */;
INSERT INTO `rel_local_compra_cds` VALUES (1,1,'2022-04-06 23:38:24'),(1,2,'2022-04-06 23:38:24'),(2,2,'2022-04-13 22:15:40'),(3,1,'2022-04-13 22:15:40'),(4,2,'2022-04-13 22:15:48');
/*!40000 ALTER TABLE `rel_local_compra_cds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rel_musicas_artista`
--

DROP TABLE IF EXISTS `rel_musicas_artista`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rel_musicas_artista` (
  `fk_musicas` int(11) NOT NULL,
  `fk_artista` int(11) NOT NULL,
  PRIMARY KEY (`fk_musicas`,`fk_artista`),
  KEY `fk_artista` (`fk_artista`),
  CONSTRAINT `rel_musicas_artista_ibfk_1` FOREIGN KEY (`fk_musicas`) REFERENCES `musicas` (`id`),
  CONSTRAINT `rel_musicas_artista_ibfk_2` FOREIGN KEY (`fk_artista`) REFERENCES `artista` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rel_musicas_artista`
--

LOCK TABLES `rel_musicas_artista` WRITE;
/*!40000 ALTER TABLE `rel_musicas_artista` DISABLE KEYS */;
INSERT INTO `rel_musicas_artista` VALUES (1,4),(2,4),(3,4),(4,2),(5,3),(6,1);
/*!40000 ALTER TABLE `rel_musicas_artista` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `respostas`
--

DROP TABLE IF EXISTS `respostas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `respostas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero_questao` int(11) DEFAULT NULL,
  `resposta` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `respostas`
--

LOCK TABLES `respostas` WRITE;
/*!40000 ALTER TABLE `respostas` DISABLE KEYS */;
INSERT INTO `respostas` VALUES (1,1,'select *from cds;'),(2,2,'select nome,data_compra from cds,rel_local_compra_cds where fk_cds=cds.id order by nome asc;'),(3,3,'select nome,data_compra from cds,rel_local_compra_cds where fk_cds=cds.id order by data_compra desc;'),(4,4,'select sum(valor_pago) as total_pago from cds;'),(5,5,'select musicas.* from musicas,cds where fk_cd=cds.id and cds.codigo=\'123456\';'),(6,6,'select cds.nome as cd,musicas.nome as nome_musica from musicas,cds where cds.id=music\nas.fk_cd;'),(7,7,'select artista.name as artista, musicas.nome as musica from artista,musicas,rel_musicas_artista where artista.id=fk_artista and musicas.id=fk_musicas;'),(8,8,'select sum(tempo) as tempo_total from musicas;'),(9,9,'select numero,musicas.nome,tempo from musicas,cds where cds.codigo=\'123456\';'),(10,10,'select cds.nome as cd, sum(tempo) as tempo_total from cds,musicas where fk_cd=cds.id\ngroup by cds.nome;\n'),(11,11,'select count(musicas.id) as quantity from musicas;'),(12,12,'select avg(tempo) as media_tempo from musicas;'),(13,13,'select count(*) as qtde_cds from cds;'),(14,14,'SELECT artista.name, musicas.nome, musicas.id FROM musicas, artista, rel_musicas_artista WHERE rel_musicas_artista.fk_musicas = musicas.id AND rel_musicas_artista.fk_artista=artista.id AND artista.name = \'Alok\''),(15,15,'select cds.id,cds.nome, count(musicas.id) from musicas,cds where musicas.fk_cd=cds.id group by cds.id;'),(16,16,'SELECT cds.nome FROM cds, rel_local_compra_cds, local_compra WHERE rel_local_compra_cds.fk_local_compra = local_compra.id AND rel_local_compra_cds.fk_cds = cds.id AND local_compra.rua = \'submarino\''),(17,17,'select cds.nome as cd,musicas.nome as musica from cds,musicas where musicas.fk_cd=cds.id and musicas.numero=1;'),(18,17,'select min(musicas.numero),cds.nome as cd,musicas.nome as musica from cds,musicas where musicas.fk_cd=cds.id group by cds.id;'),(19,18,'select *from musicas order by nome asc;'),(20,19,'select *from cds where album_id is not null;'),(21,20,'select valor_pago from cds order by valor_pago desc limit 1;'),(22,20,'select max(valor_pago) from cds;');
/*!40000 ALTER TABLE `respostas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-20 20:42:19
