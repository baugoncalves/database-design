-- MariaDB dump 10.19  Distrib 10.5.15-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: empresa
-- ------------------------------------------------------
-- Server version	10.5.15-MariaDB-0+deb11u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cidade`
--

DROP TABLE IF EXISTS `cidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) DEFAULT NULL,
  `estado_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `estado_id` (`estado_id`),
  CONSTRAINT `cidade_ibfk_1` FOREIGN KEY (`estado_id`) REFERENCES `estado` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cidade`
--

LOCK TABLES `cidade` WRITE;
/*!40000 ALTER TABLE `cidade` DISABLE KEYS */;
INSERT INTO `cidade` VALUES (1,'Teófilo otoni',1),(2,'Salvador',2);
/*!40000 ALTER TABLE `cidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departamento`
--

DROP TABLE IF EXISTS `departamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) DEFAULT NULL,
  `localizacao` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departamento`
--

LOCK TABLES `departamento` WRITE;
/*!40000 ALTER TABLE `departamento` DISABLE KEYS */;
INSERT INTO `departamento` VALUES (1,'RH','Bloco 1'),(2,'Financeiro','Bloco 2'),(3,'Recepção','Entrada'),(4,'Serviços Gerais',NULL),(5,'Administrativo','Top');
/*!40000 ALTER TABLE `departamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado`
--

DROP TABLE IF EXISTS `estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) DEFAULT NULL,
  `uf` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado`
--

LOCK TABLES `estado` WRITE;
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
INSERT INTO `estado` VALUES (1,'Minas Gerais','MG'),(2,'Bahia','BA');
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `funcao`
--

DROP TABLE IF EXISTS `funcao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `funcao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funcao`
--

LOCK TABLES `funcao` WRITE;
/*!40000 ALTER TABLE `funcao` DISABLE KEYS */;
INSERT INTO `funcao` VALUES (1,'Auxiliar Administrativo'),(2,'Supervisor'),(3,'Analista Financeiro'),(4,'Telefonista'),(5,'Gerente');
/*!40000 ALTER TABLE `funcao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `funcionario`
--

DROP TABLE IF EXISTS `funcionario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `funcionario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `primeiro_nome` varchar(15) DEFAULT NULL,
  `segundo_nome` varchar(15) DEFAULT NULL,
  `ultimo_nome` varchar(15) DEFAULT NULL,
  `data_nascimento` date DEFAULT NULL,
  `cpf` varchar(11) DEFAULT NULL,
  `rg` varchar(10) DEFAULT NULL,
  `rua` varchar(30) DEFAULT NULL,
  `numero` varchar(5) DEFAULT NULL,
  `complemento` varchar(30) DEFAULT NULL,
  `bairro` varchar(30) DEFAULT NULL,
  `cep` varchar(9) DEFAULT NULL,
  `cidade_id` int(11) DEFAULT NULL,
  `fone` varchar(15) DEFAULT NULL,
  `departamento_id` int(11) DEFAULT NULL,
  `funcao_id` int(11) DEFAULT NULL,
  `salario` decimal(7,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cidade_id` (`cidade_id`),
  KEY `departamento_id` (`departamento_id`),
  KEY `funcionario_ibfk_3` (`funcao_id`),
  CONSTRAINT `funcionario_ibfk_1` FOREIGN KEY (`cidade_id`) REFERENCES `cidade` (`id`),
  CONSTRAINT `funcionario_ibfk_2` FOREIGN KEY (`departamento_id`) REFERENCES `departamento` (`id`),
  CONSTRAINT `funcionario_ibfk_3` FOREIGN KEY (`funcao_id`) REFERENCES `funcao` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funcionario`
--

LOCK TABLES `funcionario` WRITE;
/*!40000 ALTER TABLE `funcionario` DISABLE KEYS */;
INSERT INTO `funcionario` VALUES (1,'Fulano','de','Tal','1990-05-31','12345678911','123212312','Avenida Teste','2','Casa','Novo Bairro','3980000',1,'33988695277',1,1,1500.00),(2,'Ciclano',NULL,'Silva','2005-03-07','123133123','22222','Street View','12','Apartamento','Duradouro','39800370',2,'123123222',5,4,4380.00),(3,'Betrano','de','Lá','2000-01-12','2121221','sdd22','Crua a rua','212','Perto','Bairo de Lá','39800547',1,'21234555',5,5,2500.00);
/*!40000 ALTER TABLE `funcionario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resposta`
--

DROP TABLE IF EXISTS `resposta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resposta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` int(11) DEFAULT NULL,
  `descricao` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resposta`
--

LOCK TABLES `resposta` WRITE;
/*!40000 ALTER TABLE `resposta` DISABLE KEYS */;
INSERT INTO `resposta` VALUES (1,1,'select primeiro_nome,segundo_nome,ultimo_nome from funcionario order by ultimo_nome;'),(2,1,'select CONCAT(primeiro_nome,,segundo_nome,,ultimo_nome) as nome_completo from funcionario order by ultimo_nome;'),(3,2,'select funcionario.* from funcionario inner join cidade on cidade.id=funcionario.cidade_id order by cidade.nome;'),(4,3,'select CONCAT(primeiro_nome, \" \",segundo_nome,\" \",ultimo_nome) as nome_completo from funcionario where funcionario.salario > 1000 order by nome_completo asc;'),(5,4,'select primeiro_nome,data_nascimento from funcionario order by data_nascimento desc;'),(6,5,'select sum(salario) as total_folha from funcionario;'),(7,6,'SELECT funcionario.primeiro_nome, departamento.nome as departamento, funcao.descricao as funcao FROM departamento INER JOIN funcionario ON funcionario.departamento_id = departamento.id INNER JOIN funcao ON funcionario.funcao_id = funcao.id'),(9,7,'SELECT\r\n    departamento.nome,\r\n    CASE\r\n    	WHEN funcionario.primeiro_nome is NULL THEN \"Sem gerente\"\r\n       	ELSE funcionario.primeiro_nome\r\n    END as gerente\r\nFROM\r\n    funcionario\r\n	INNER JOIN funcao ON funcionario.funcao_id = funcao.id AND funcao.descricao = \'gerente\'\r\n	RIGHT JOIN departamento ON funcionario.departamento_id = departamento.id;'),(10,8,'select departamento.nome, sum(salario) from funcionario right join departamento on funcionario.departamento_id=departamento.id group by departamento.nome;\r\n'),(11,9,'SELECT\r\n    departamento.nome,\r\n    CASE\r\n    	WHEN funcionario.primeiro_nome is NULL THEN \"Sem supervisor\"\r\n       	ELSE funcionario.primeiro_nome\r\n    END as supervisor\r\nFROM\r\n    funcionario\r\n	INNER JOIN funcao ON funcionario.funcao_id = funcao.id AND funcao.descricao = \'supervisor\'\r\n	RIGHT JOIN departamento ON funcionario.departamento_id = departamento.id;'),(12,10,'select count(*) as total_funcionario from funcionario;'),(13,11,'select avg(salario) as media_salario from funcionario;'),(14,12,'select departamento.nome, min(salario) from funcionario RIGHT join departamento on funcionario.departamento_id=departamento.id group by departamento.nome;\r\n'),(15,13,'select CONCAT(primeiro_nome, \' \', ultimo_nome) as nome_completo from funcionario where segundo_nome is NULL or segundo_nome=\'\';\r\n'),(16,14,'select departamento.nome, funcionario.primeiro_nome from departamento LEFT JOIN funcionario on funcionario.departamento_id=departamento.id order by departamento.nome,funcionario.primeiro_nome desc;\r\n'),(17,15,'select funcionario.primeiro_nome from funcionario inner join funcao on funcao.id=funcionario.funcao_id inner join cidade on funcionario.cidade_id=cidade.id inner join estado on estado.id=cidade.estado_id where estado.uf=\'BA\' and funcao.descricao=\'Telefonista\';'),(18,16,'select funcionario.primeiro_nome from funcionario inner join departamento on departamento.id=funcionario.departamento_id where departamento.nome=\'rh\';'),(19,17,'SELECT\r\n    funcionario.primeiro_nome,\r\n    departamento.nome,\r\n    funcionario.salario\r\nFROM\r\n    funcionario\r\n	INNER JOIN departamento ON funcionario.departamento_id = departamento.id\r\nWHERE\r\n    funcionario.salario > (\r\n        SELECT\r\n            funcionario.salario\r\n        FROM\r\n        	funcionario\r\n        	INNER JOIN funcao ON funcionario.funcao_id = funcao.id\r\n        WHERE\r\n        	funcao.descricao = \'gerente\'\r\n	);');
/*!40000 ALTER TABLE `resposta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-06-01 19:27:39
